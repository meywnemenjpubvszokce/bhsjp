const sanitizeHtml = require("sanitize-html");

function filterHtml(html) {
    return sanitizeHtml(html, {
        allowedTags: [
            'p',
            'strong',
            'em',
            'u',
            'a',
            'br',
            'ol',
            'ul',
            'li',
            'h1',
            'h2',
            'h3',
            'br'
        ],
        allowedAttributes: {
            'a': [
                'href'
            ]
        },
        allowedClasses: {},
        allowedStyles: {}
    });
}

module.exports = {
    filterHtml: filterHtml
};
