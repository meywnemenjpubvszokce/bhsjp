const mysql = require('mysql');
const escapeHtml = require('escape-html');
const MysqlData = require('../models/MysqlData');
const connection = mysql.createConnection(new MysqlData().getConnection());
const Sha512 = require('../lib/Sha512');
const Aes256 = require('../lib/Aes256');

module.exports = class Account {
    create(id, password, nickname, email) {
        return new Promise(resolve => {
            connection.query("INSERT INTO `accounts` (`id`, `password`, `nickname`, `email`, `isVerified`) VALUES (?, ?, ?, ?, FALSE)", [
                new Aes256(escapeHtml(id), 'plain').getEncrypted(),
                new Sha512(password).getEncrypted(),
                new Aes256(escapeHtml(nickname), 'plain').getEncrypted(),
                new Aes256(escapeHtml(email), 'plain').getEncrypted()
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                } else {
                    resolve('success');
                }
            });
        });
    }

    doAccountExist(id, password) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT * FROM accounts WHERE `id`=? AND `password`=?', [
                new Aes256(escapeHtml(id), 'plain').getEncrypted(),
                new Sha512(password).getEncrypted()
            ], (error, results, fields) => {
                if (error) {
                    throw error;
                }

                if (results.length > 0) {
                    resolve(results[0]);
                } else {
                    reject('account-not-exist');
                }
            });
        });
    }

    doIdExist(id) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT COUNT(1) count FROM accounts WHERE `id`=?', [
                new Aes256(escapeHtml(id), 'plain').getEncrypted()
            ], (error, results, fields) => {
                if (error) {
                    throw error;
                }

                if (results[0].count) {
                    resolve('id-not-exist');
                } else {
                    reject('id-exist');
                }
            });
        });
    }

    doEmailExist(email) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT COUNT(1) count FROM accounts WHERE `email`=?', [
                new Aes256(escapeHtml(email), 'plain').getEncrypted()
            ], (error, results, fields) => {
                if (error) {
                    throw error;
                }

                if (results[0].count) {
                    resolve('email-not-exist');
                } else {
                    reject('email-exist');
                }
            });
        });
    }

    doNicknameExist(nickname) {
        return new Promise((resolve, reject) => {
            connection.query('SELECT COUNT(1) count FROM accounts WHERE `nickname`=?', [
                new Aes256(escapeHtml(nickname), 'plain').getEncrypted()
            ], (error, results, fields) => {
                if (error) {
                    throw error;
                }

                if (results[0].count) {
                    resolve('nickname-not-exist');
                } else {
                    reject('nickname-exist');
                }
            });
        });
    }

    isVerified(index) {
        return new Promise((resolve, reject) => {
            connection.query("SELECT `isVerified`\n" +
                "    FROM `accounts`\n" +
                "    WHERE `index`=?;", [
                index
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                }

                if (result.length > 0) {
                    resolve(!!result[0].isVerified);
                } else {
                    reject('no-account');
                }
            })
        });
    }

    setToVerified(index) {
        return new Promise(resolve => {
            connection.query("UPDATE `accounts`\n" +
                "    SET\n" +
                "        `isVerified`=TRUE\n" +
                "    WHERE `index`=?;", [
                index
            ], (error, result, fields) => {
                if (error) {
                     throw error;
                }

                resolve();
            });
        });
    }

    getDataByIndex(index) {
        return new Promise((resolve, reject) => {
            connection.query("SELECT *\n" +
                "    FROM `accounts`\n" +
                "    WHERE `index`=?;", [
                index
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                }

                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject('no-row');
                }
            })
        });
    }

    getIndexByEncryptedId(encryptedId) {
        return new Promise((resolve, reject) => {
            connection.query("SELECT `index`\n" +
                "    FROM `accounts`\n" +
                "    WHERE `id`=?;", [
                encryptedId
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                }

                if (result.length > 0) {
                    resolve(result[0].index);
                } else {
                    reject('no-row');
                }
            })
        });
    }

    getIndexByEncryptedEmail(email) {
        return new Promise((resolve, reject) => {
            connection.query("SELECT `index`\n" +
                "    FROM `accounts`\n" +
                "    WHERE `email`=?;", [
                email
            ], (error, result, fields) => {
                if (error) {
                     throw error;
                }

                if (result.length > 0) {
                    resolve(result[0].index);
                } else {
                    reject('no-row');
                }
            });
        });
    }

    getData(json) {
        return new Promise((resolve, reject) => {
            let whereList = [];
            const jsonLength = Object.keys(json).length;

            if (jsonLength === 0) {
                throw new Error('Parameter does not have data');
            }

            for (const i in json) {
                whereList.push(i);
                whereList.push(json[i]);
            }

            connection.query("SELECT *\n" +
                "    FROM `accounts`\n" +
                "    WHERE " +
                Array(jsonLength).fill('??=?').join(' AND\n'),
                whereList, (error, result, fields) => {
                if (error) {
                    throw error;
                }

                if (result.length > 0) {
                    resolve(result);
                } else {
                    reject('no-row');
                }
            });
        });
    }

    setData(data, where) {
        return new Promise(resolve => {
            let whereList = [];
            const whereLength = Object.keys(where).length;

            for (const i in where) {
                whereList.push(i);
                whereList.push(where[i]);
            }

            connection.query("UPDATE `accounts`\n" +
                "    SET ?\n" +
                "    WHERE " + Array(whereLength).fill('??=?').join(' AND '),
                [data].concat(whereList),
            (error, result, fields) => {
                if (error) {
                    throw error;
                }

                resolve();
            });
        });
    }
};
