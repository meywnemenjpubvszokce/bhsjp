const mysql = require('mysql');
const MysqlData = require('../models/MysqlData');
const connection = mysql.createConnection(new MysqlData().getConnection());
const filterHtml = require('../lib/filterHtml').filterHtml;

module.exports = class Comment {
    constructor() {}

    create(userIndex, postId, content, isPrivate) {
        return new Promise(resolve => {
            connection.query("INSERT INTO comments\n" +
                "    (author, postId, content, date, isPrivate, isModified)\n" +
                "    VALUES (?, ?, ?, NOW(), ?, ?);", [
                userIndex,
                postId,
                filterHtml(content),
                isPrivate,
                false
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                }

                resolve();
            });
        });
    }

    update(userIndex, commentId, content, isPrivate) {
        return new Promise((resolve, reject) => {
            connection.query("SELECT author\n" +
                "    FROM comments\n" +
                "    WHERE `index`=?;", [
                commentId
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                }

                if (result.length > 0) {
                    if (result[0].author === userIndex) {
                        connection.query("UPDATE comments\n" +
                            "    SET\n" +
                            "        content=?,\n" +
                            "        date=NOW(),\n" +
                            "        `isPrivate`=?,\n" +
                            "        `isModified`=TRUE\n" +
                            "    WHERE `index`=?;", [
                            filterHtml(content),
                            isPrivate,
                            commentId
                        ], (error, result, fields) => {
                            if (error) {
                                throw error;
                            }

                            resolve();
                        });
                    } else {
                        reject('invalid-user');
                    }
                } else {
                    reject('no-comment')
                }
            });
        });
    }

    delete(userIndex, commentId) {
        return new Promise((resolve, reject) => {
            connection.query("SELECT\n" +
                "    (SELECT `author`\n" +
                "        FROM `comments` c\n" +
                "        WHERE\n" +
                "            `index`=?) commentAuthor,\n" +
                "    (SELECT `author`\n" +
                "        FROM `posts` p\n" +
                "        WHERE\n" +
                "            `index`=(\n" +
                "                SELECT `postId`\n" +
                "                    FROM `comments` c\n" +
                "                    WHERE c.`index`=?\n" +
                "            )\n" +
                "    ) postOwner;\n", [
                commentId,
                commentId
            ], (error, result, fields) => {
                if (error) {
                    throw error;
                }

                if (result.length > 0) {
                    if (userIndex === result[0].commentAuthor ||
                        userIndex === result[0].postOwner) {
                        connection.query("DELETE\n" +
                            "    FROM `comments`\n" +
                            "    WHERE `index`=?;", [
                            commentId
                        ], (error, result, fields) => {
                            if (error) {
                                throw error;
                            }

                            resolve();
                        });
                    } else {
                        reject('invalid-user');
                    }
                } else {
                    reject('no-row');
                }
            })
        });
    }
};
