function fetchChangePassword(verificationCode, password) {
    fetch('https://accounts.bhsjp.kro.kr/change-password', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            verificationCode: verificationCode,
            password: password
        })
    }).then(res => {
        return res.text();
    }).then(text => {
        switch (text) {
            case 'ok':
                vex.dialog.alert({
                    unsafeMessage: messages.information.passwordChanged,
                    callback: () => {
                        location.href = 'https://accounts.bhsjp.kro.kr/sign-in';
                    }
                });

                break;

            case 'password-short':
                vex.dialog.alert({
                    unsafeMessage: messages.error.passwordLength
                });

                break;

            case 'error':
                vex.dialog.alert({
                    unsafeMessage: messages.error.server
                });

                break;
        }
    }).catch(err => {
        console.error(err);

        vex.dialog.alert({
            unsafeMessage: messages.error.cannotConnectServer
        });
    });
}