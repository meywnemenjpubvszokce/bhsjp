function changeNickname(nickname) {
    if (!(1 <= nickname.length && nickname.length <= 10)) {
        vex.dialog.alert({
            unsafeMessage: messages.error.nicknameLength
        });
    } else {
        fetch('https://accounts.bhsjp.kro.kr/change-personal-information/nickname', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                nickname: nickname
            })
        }).then(res => {
            return res.text()
        }).then(text => {
            switch (text) {
                case 'ok':
                    vex.dialog.alert({
                        unsafeMessage: messages.information.nicknameChanged,
                        callback: () => {
                            location.href = 'https://accounts.bhsjp.kro.kr/sign-in'
                        }
                    });

                    break;

                case 'short':
                case 'long':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.nicknameLength
                    });

                case 'exist':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.nicknameAlreadyTaken
                    });

                    break;

                case 'error':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.server
                    });

                    break;
            }
        }).catch(() => {
            vex.dialog.alert({
                unsafeMessage: messages.error.cannotConnectServer
            });
        });
    }
}

function changePassword(password, passwordConfirm) {
    if (password !== passwordConfirm) {
        vex.dialog.alert({
            unsafeMessage: messages.error.passwordNotMatch
        });
    } else if (password.length < 4) {
        vex.dialog.alert({
            unsafeMessage: messages.error.passwordLength
        });
    } else {
        fetch('https://accounts.bhsjp.kro.kr/change-personal-information/password', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                password: password
            })
        }).then(res => {
            return res.text();
        }).then(text => {
            switch(text) {
                case 'ok':
                    vex.dialog.alert({
                        unsafeMessage: messages.information.passwordChanged,
                        callback: () => {
                            location.href = 'https://accounts.bhsjp.kro.kr/sign-in'
                        }
                    });

                    break;

                case 'short':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.passwordLength
                    });

                    break;

                case 'error':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.server
                    });

                    break;
            }
        }).catch(() => {
            vex.dialog.alert({
                unsafeMessage: messages.error.cannotConnectServer
            });
        });
    }
}

function changeEmail(email) {
    if (!(3 <= email.length && email.length <= 320)) { // 3: /.@./
        vex.dialog.alert({
            unsafeMessage: messages.error.emailLength
        });
    } else if (email.match(/^[^@]{1,64}@[^@]{1,255}$/) === null) {
        vex.dialog.alert({
            unsafeMessage: messages.error.emailTemplate
        });
    } else {
        fetch('https://accounts.bhsjp.kro.kr/change-personal-information/email', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        }).then(res => {
            return res.text();
        }).then(text => {
            switch(text) {
                case 'ok':
                    vex.dialog.alert({
                        unsafeMessage: messages.information.emailChanged,
                        callback: () => {
                            location.href = 'https://accounts.bhsjp.kro.kr/sign-in'
                        }
                    });

                    break;

                case 'short':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.emailLength
                    });

                    break;

                case 'template-not-match':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.emailTemplate
                    });

                    break;

                case 'exist':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.emailAlreadyTaken
                    });
                    
                    break;

                case 'error':
                    vex.dialog.alert({
                        unsafeMessage: messages.error.server
                    });

                    break;
            }
        }).catch(() => {
            vex.dialog.alert({
                unsafeMessage: messages.error.cannotConnectServer
            });
        });
    }
}
