const toggleSubMenu = (e, topMenuIcon, subMenu) => {
    const eTargetDataset = e.target.dataset;

    if (eTargetDataset !== null) {
        if (eTargetDataset.isOpened === 'true') {
            topMenuIcon.innerHTML = 'keyboard_arrow_right';
            subMenu.style.height = '0px';
            eTargetDataset.isOpened = 'false';
        } else {
            topMenuIcon.innerHTML = 'keyboard_arrow_down';
            subMenu.style.height = 'auto';
            eTargetDataset.isOpened = 'true';
        }
    }
};
