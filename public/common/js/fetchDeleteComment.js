const fetchDeleteComment = commentId => {
    fetch('https://community.bhsjp.kro.kr/delete-comment', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            commentId: commentId
        })
    }).then(res => {
        return res.text();
    }).then(text => {
        switch (text) {
            case 'ok':
                vex.dialog.alert({
                    unsafeMessage: messages.information.commentDeleted,
                    callback: () => {
                        location.reload();
                    }
                });

                break;

            case 'invalid-user':
                vex.dialog.alert({
                    unsafeMessage: messages.error.commentDeleteNoPermission
                });

                break;

            case 'no-row':
                vex.dialog.alert({
                    unsafeMessage: messages.error.noComment
                });

                break;
            case 'error':
                vex.dialog.alert({
                    unsafeMessage: messages.error.server
                });

                break;
        }
    }).catch(err => {
        console.error(err);

        vex.dialog.alert({
            unsafeMessage: messages.error.cannotConnectServer
        });
    });
};
