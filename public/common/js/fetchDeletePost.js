const fetchDeletePost = (postId, password) => {
    fetch('https://community.bhsjp.kro.kr/delete-post', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            postId: postId,
            password: password
        })
    }).then(res => {
        return res.text();
    }).then(text => {
        switch (text) {
            case 'ok':
                vex.dialog.alert({
                    unsafeMessage: messages.information.postDeleted,
                    callback: () => {
                        location.href = 'https://community.bhsjp.kro.kr/view-posts/0';
                    }
                });

                break;

            case 'wrong-post':
                vex.dialog.open({
                    message: messages.request.inputPassword,
                    input: `
                        <input name="password" type="password">
                    `,
                    callback: data => {
                        if (data) {
                            fetchDeletePost(postId, data.password);
                        } else {
                            vex.dialog.alert({
                                unsafeMessage: messages.information.postDeletionCanceled
                            });
                        }
                    }
                });

                break;

            case 'invalid-user':
                vex.dialog.alert({
                    unsafeMessage: messages.error.notPostOwner
                });

                break;

            case 'error':
                vex.dialog.alert({
                    unsafeMessage: messages.error.server,
                    callback: () => {
                        vex.dialog.open({
                            message: messages.request.inputPassword,
                            input: `
                            <input name="password" type="password">
                        `,
                            callback: (data) => {
                                if (data) {
                                    fetchPost(postId, data.password);
                                } else {
                                    history.back()
                                }
                            }
                        });
                    }
                });

                break;
        }
    }).catch(err => {
        console.error(err);

        vex.dialog.alert({
            unsafeMessage: messages.error.cannotConnectServer
        });
    });
};
