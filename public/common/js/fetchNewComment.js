const fetchNewComment = callback => {
    fetch('https://community.bhsjp.kro.kr/create-comment', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            postId: postId,
            content: quill.container.firstChild.innerHTML,
            isPrivateComment: isPrivateComment.checked
        })
    }).then(res => {
        return res.text();
    }).then(text => {
        switch (text) {
            case 'not-signed-in':
                vex.dialog.alert({
                    unsafeMessage: messages.error.notSignedIn,
                    callback: () => {
                        location.href = 'https://accounts.bhsjp.kro.kr/sign-in';
                    }
                });

                break;

            case 'empty-content':
                vex.dialog.alert({
                    unsafeMessage: messages.error.emptyComment
                });

                break;

            case 'error':
                vex.dialog.alert({
                    unsafeMessage: messages.error.server
                });

                break;

            case 'ok':
                vex.dialog.alert({
                    unsafeMessage: messages.information.commentRegistered,
                    callback: () => {
                        location.reload();
                    }
                });
        }
    }).catch(err => {
        console.error(err);

        vex.dialog.alert({
            unsafeMessage: messages.error.cannotConnectServer
        });
    });

    callback();
};
