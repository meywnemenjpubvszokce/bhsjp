const fetchPostToFix = (postId, password) => {
    fetch('https://community.bhsjp.kro.kr/get-post', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            postId: postId,
            password: password
        })
    }).then(res => {
        return res.json();
    }).then(obj => {
        switch (obj.result) {
        case 'right':
            window.userPassword = password;

            postTitle.value = obj.data.title;
            quill.clipboard.dangerouslyPasteHTML(obj.data.content);

            break;

        case 'wrong':
            vex.dialog.open({
                message: messages.request.inputPassword,
                input: `
                        <input name="password" type="password">
                    `,
                callback: data => {
                    if (data) {
                        fetchPostToFix(postId, data.password);
                    } else {
                        history.back()
                    }
                }
            });

            break;

        case 'no-post':
            vex.dialog.alert({
                unsafeMessage: messages.error.noPost,
                callback: () => {
                    history.back();
                }
            });

            break;

        case 'error':
            vex.dialog.alert({
                unsafeMessage: messages.error.server,
                callback: () => {
                    vex.dialog.open({
                        message: messages.request.inputPassword,
                        input: `
                            <input name="password" type="password">
                        `,
                        callback: (data) => {
                            if (data) {
                                fetchPost(postId, data.password);
                            } else {
                                history.back()
                            }
                        }
                    });
                }
            });

            break;
        }
    }).catch(err => {
        console.error(err);

        vex.dialog.alert({
            unsafeMessage: messages.error.cannotConnectServer
        });
    });
};
