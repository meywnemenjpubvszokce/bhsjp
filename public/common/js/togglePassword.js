const togglePassword = (passwordToggle, passwordToggleIcon, target) => {
    passwordToggle.addEventListener('click', () => {
        if (passwordToggle.dataset.isShowing === 'true') {
            passwordToggleIcon.innerHTML = 'visibility';
            target.type = 'password';
            passwordToggle.dataset.isShowing = 'false';
        } else {
            passwordToggleIcon.innerHTML = 'visibility_off';
            target.type = 'text';
            passwordToggle.dataset.isShowing = 'true';
        }
    });
};
