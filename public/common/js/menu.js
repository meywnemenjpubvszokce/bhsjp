document.addEventListener('DOMContentLoaded', () => {
    const menuBar = document.getElementById('menu-bar');
    const menuIcon = document.getElementById('menu-icon');
    const topMenus = document.getElementsByClassName('top-menus');
    const topMenuIcons = document.getElementsByClassName('top-menu-icons');
    const subMenus = document.getElementsByClassName('sub-menus');

    if (menuIcon !== null && menuBar != null) {
        menuIcon.addEventListener('click', () => {
            if (menuBar.dataset.isOpened === 'true') {
                menuBar.style.width = '0px';
                menuBar.dataset.isOpened = 'false';
            } else {
                menuBar.style.width = window.innerWidth < 600 ? '100vw' : '200px';
                menuBar.dataset.isOpened = 'true';
            }
        });
    }

    for (let i = 0; i < topMenus.length; i++) {
        topMenus[i].addEventListener('click', e => toggleSubMenu(e, topMenuIcons[i], subMenus[i]));
    }
});
