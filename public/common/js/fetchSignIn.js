const fetchSignIn = callback => {
    fetch('/check-account', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: accountId.value,
            password: accountPassword.value
        })
    }).then(res => {
        return res.text();
    }).then(text => {
        callback();

        switch (text) {
            case 'no-id':
                vex.dialog.alert({
                    unsafeMessage: messages.error.noId
                });

                break;

            case 'id-length-short':
            case 'id-length-long':
                vex.dialog.alert({
                    unsafeMessage: messages.error.idLength
                });

                break;

            case 'id-template-not-match':
                vex.dialog.alert({
                    unsafeMessage: messages.error.idTemplate
                });

                break;

            case 'no-password':
                vex.dialog.alert({
                    unsafeMessage: messages.error.noPassword
                });

                break;

            case 'password-length-short':
                vex.dialog.alert({
                    unsafeMessage: messages.error.passwordLength
                });

                break;

            case 'wrong':
                vex.dialog.alert({
                    unsafeMessage: messages.err.idOrPasswordWrong
                });

                break;

            case 'already-signed-in':
                vex.dialog.alert({
                    unsafeMessage: messages.error.alreadySignedIn
                });

                break;

            case 'not-verified':
                vex.dialog.alert({
                    unsafeMessage: messages.error.notVerified
                });

                break;

            case 'error':
                vex.dialog.alert({
                    unsafeMessage: messages.error.server
                });

                break;
            
            case 'ok':
                vex.dialog.alert({
                    unsafeMessage: messages.information.successfullySignedIn,
                    callback: () => {
                        location.href = 'https://bhsjp.kro.kr';
                    }
                });

                break;
        }
    }).catch(err => {
        console.error(err);

        vex.dialog.alert({
            unsafeMessage: messages.error.cannotConnectServer
        });
    });
};
