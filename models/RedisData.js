const redis = require('redis');
const redisClient = redis.createClient();

redisClient.on('error', err => {
    if (err) {
        throw err;
    }
});

module.exports = class RedisData {
    constructor() {
        this.client = redisClient;
    }

    getClient() {
        return this.client;
    }
};
